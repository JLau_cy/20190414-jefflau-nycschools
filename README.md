# Project settings
- Supported form factors - Universal
- Deployment target - 12.2
- Language - Swift 5
- IDE - Xcode 10.2

# Assumptions
- The app only requires two HTTP GET calls.
  - Get schools
  - Get SAT scores for school
- No offline capability required.
  
# Design choices
- As this is a simple app with very few API calls, I have opted to make a very light weight **HttpRequest** protocol with protocol extension to handle network requests.
  - The protocol oriented design and the usage of generics in **HttpRequest** makes it very easy to scale to support other HTTP methods as well as create new API requests.
- Used Swfitgen to generate enums for String literals in the app.
  - For more information: [Swiftgen Github page](https://github.com/SwiftGen/SwiftGen)
- The project is split into 3 main areas of Network, Models and Core App represented in folder structures.
  - In practice, depending on the scale of the app and number of apps within an organisation, Network and Models may exist as .framework or managed by separate projects to enhance reusable code within an organisation.
  - **Network** - Contains app independant networking code that can be reused. Typically this would be a protocol oriented abstraction layer on top of iOS networking APIs or abstraction layer over a third party networking framework such as Alamofire. Utilising a protocol oriented abstraction layer will allow changes to the underlying network layer without causing mass code changes across an app.
  - **Models** - Contains model code that represents end point response objects.
  - **App Code** - Feature code that builds the app's functionality utilising code within Network and Models.
- To further separate responsibility, MVP was chosen as the design pattern. This has allowed both the Model's and Presenter's essential code to be fully unit tested.
  - A Slather report can be found in $(PROJECT_ROOT)/Slather/index.html that shows a 96.33% test coverage of business logic code.

## Happy to answer any questions you may have regarding the code or to clarify decisions made.