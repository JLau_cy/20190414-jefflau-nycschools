//
//  SatScore.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

struct SatScore: Decodable {
    enum CodingKeys: String, CodingKey {
        case math = "sat_math_avg_score"
        case reading = "sat_critical_reading_avg_score"
        case writing = "sat_writing_avg_score"
    }
    
    let math: Double
    let reading: Double
    let writing: Double
    
    init(from decoder: Decoder) throws {
        //Since the scores are comming from the API as a string, I am ensuring the strings are convertible to a Double otherwise throwing a data corrupted error.
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let mathScoreString = try values.decode(String.self, forKey: .math)
        guard let mathScore = Double(mathScoreString) else {
            throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [CodingKeys.math], debugDescription: "SAT score is not a number"))
        }
        math = mathScore
        
        let readingScoreString = try values.decode(String.self, forKey: .reading)
        guard let readingScore = Double(readingScoreString) else {
            throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [CodingKeys.reading], debugDescription: "SAT score is not a number"))
        }
        reading = readingScore
        
        let writingScoreString = try values.decode(String.self, forKey: .writing)
        guard let writingScore = Double(writingScoreString) else {
            throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [CodingKeys.math], debugDescription: "SAT score is not a number"))
        }
        writing = writingScore
        
    }
}
