//
//  School.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

final class School: Decodable {
    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case overview = "overview_paragraph"
        case addressLine1 = "primary_address_line_1"
        case neighborhood
        case city
        case zip
        case stateCode = "state_code"
        case latitude
        case longitude
        case phoneNumber = "phone_number"
        case email = "school_email"
        case website
    }
    
    let dbn: String
    let name: String
    let overview: String
    let addressLine1: String
    let neighborhood: String
    let city: String
    let zip: String
    let stateCode: String
    let latitude: Double?
    let longitude: Double?
    let phoneNumber: String
    let email: String?
    let website: URL?
    var satScore: SatScore?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try values.decode(String.self, forKey: .dbn)
        name = try values.decode(String.self, forKey: .name)
        overview = try values.decode(String.self, forKey: .overview)
        addressLine1 = try values.decode(String.self, forKey: .addressLine1)
        neighborhood = try values.decode(String.self, forKey: .neighborhood)
        city = try values.decode(String.self, forKey: .city)
        zip = try values.decode(String.self, forKey: .zip)
        stateCode = try values.decode(String.self, forKey: .stateCode)
        phoneNumber = try values.decode(String.self, forKey: .phoneNumber)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        
        do {
            if let lat = try values.decodeIfPresent(String.self, forKey: .latitude),
                let lon = try values.decodeIfPresent(String.self, forKey: .longitude) {
                self.latitude = Double(lat)
                self.longitude = Double(lon)
            } else {
                self.latitude = nil
                self.longitude = nil
            }
        } catch {
            latitude = nil
            longitude = nil
        }
        
        //One school has an invalid URL format for the website field.
        //Typically I would request the backend data to be fixed. However in this excercise I am catering for it in the app by assigning nil for invalid websites.
        do {
            website = try values.decode(URL.self, forKey: .website)
        } catch {
            website = nil
        }
    }
}
