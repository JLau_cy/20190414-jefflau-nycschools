//
//  SchooListViewController.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

final class SchoolListViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var tableViewDataSource: SchoolListTableViewDataSource!
    
    private let refreshControl = UIRefreshControl()
    
    let presenter = SchoolListPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = presenter.title
        
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(presenter, action: #selector(SchoolListPresenter.loadSchools), for: .valueChanged)
        
        presenter.delegate = self
        tableViewDataSource.presenter = presenter
        presenter.loadSchools()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
}

extension SchoolListViewController: SchoolListPresenterDelegate {
    func didLoadSchools(_ schools: [School]) {
        refreshControl.endRefreshing()
        tableView.reloadData()
    }
    
    func didFailToLoadSchools(message: String) {
        refreshControl.endRefreshing()
        let action = UIAlertAction(title: Localizable.Common.Alert.Action.Confirm.title, style: .cancel, handler: nil)
        let alertController = UIAlertController(title: Localizable.Common.Alert.TechicalError.title, message: message, preferredStyle: .alert)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
