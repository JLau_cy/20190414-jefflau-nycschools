//
//  SchoolListTableViewDelegate.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

extension SchoolListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailsPresenter = presenter.detailsPresenter(for: indexPath.row) else { return }
        let detailsViewController = StoryboardScene.SchoolList.schoolDetailsVcId.instantiate()
        detailsViewController.presenter = detailsPresenter
        showDetailViewController(detailsViewController, sender: nil)
    }
}
