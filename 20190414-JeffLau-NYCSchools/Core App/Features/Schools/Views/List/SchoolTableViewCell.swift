//
//  SchoolTableViewCell.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

final class SchoolTableViewCell: UITableViewCell {
    static let cellId = "SchoolCellId"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var neighborhoodLabel: UILabel!
    
    func configure(name: String, neighborhood: String) {
        nameLabel.text = name
        neighborhoodLabel.text = neighborhood
    }
}
