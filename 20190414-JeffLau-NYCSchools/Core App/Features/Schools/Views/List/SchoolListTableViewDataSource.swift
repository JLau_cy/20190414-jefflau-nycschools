//
//  SchoolListTableViewDatasource.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

final class SchoolListTableViewDataSource: NSObject, UITableViewDataSource {
    weak var presenter: SchoolListPresenter!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfSchools()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let schoolNameAndNeighborhood = presenter.schoolNameAndNeighborhood(for: indexPath.row) else { return UITableViewCell() }
        let cell: SchoolTableViewCell = tableView.dequeueReusableCell(withIdentifier: SchoolTableViewCell.cellId, for: indexPath)
        cell.configure(name: schoolNameAndNeighborhood.name, neighborhood: schoolNameAndNeighborhood.neighborhood)
        return cell
    }
}
