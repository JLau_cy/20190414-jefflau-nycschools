//
//  MapTableViewCell.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit
import MapKit

final class MapTableViewCell: UITableViewCell {
    static let cellId = "MapCellId"
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapViewHeightConstraint: NSLayoutConstraint!
    
    /// Adds an annotation to the map with the coordinate and centers on the annotation.
    ///
    /// - Parameter mapLocation: The map location detail to add annotation
    func addSchoolAnnotationToMap(_ mapLocation: (coordinate: CLLocationCoordinate2D, latDistance: Double, lonDistance: Double)) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = mapLocation.coordinate
        
        let viewRegion = MKCoordinateRegion(center: mapLocation.coordinate,
                                            latitudinalMeters: mapLocation.latDistance,
                                            longitudinalMeters: mapLocation.lonDistance)
        
        mapView.addAnnotation(annotation)
        mapView.setRegion(viewRegion, animated: true)
    }
}
