//
//  ImageAndTitleTableViewCell.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

final class ImageAndTitleTableViewCell: UITableViewCell {
    static let cellId = "ImageAndTitleCellId"
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func configure(with title: String, icon: UIImage) {
        titleLabel.text = title
        iconView.image = icon
    }
}
