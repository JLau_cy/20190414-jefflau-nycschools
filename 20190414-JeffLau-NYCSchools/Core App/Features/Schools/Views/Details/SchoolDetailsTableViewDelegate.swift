//
//  SchoolDetailsTableViewDelegate.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

extension SchoolDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //Fixes autolayout issue where map view cell's dynamic height cannot be correctly calculated by the system and is out 0.1.
        if let mapCell = cell as? MapTableViewCell,
            mapCell.contentView.bounds.height != mapCell.mapViewHeightConstraint.constant {
            mapCell.mapViewHeightConstraint.constant = mapCell.contentView.bounds.height
        }
    }
}
