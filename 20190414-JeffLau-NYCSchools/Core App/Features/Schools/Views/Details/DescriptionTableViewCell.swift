//
//  DescriptionTableViewCell.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

final class DescriptionTableViewCell: UITableViewCell {
    static let cellId = "DescriptionCellId"
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure(with description: String) {
        descriptionLabel.text = description
    }
}
