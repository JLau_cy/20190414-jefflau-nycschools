//
//  SchoolDetailsViewController.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit
import MapKit

final class SchoolDetailsViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var tableViewDataSource: SchoolDetailsTableViewDataSource!
    
    var presenter: SchoolDetailsPresenter?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
        presenter?.delegate = self
        tableViewDataSource.presenter = presenter
        presenter?.loadSatScores()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        navigationController?.navigationBar.prefersLargeTitles = false
    }
}

extension SchoolDetailsViewController: SchoolDetailsPresenterDelegate {
    func didLoadSatScore(_ score: SatScore?, isFromCache: Bool) {
        let numberOfSections = tableView.numberOfSections
        
        switch (numberOfSections, score) {
        case (2, .some(_)):
            tableView.insertSections([SchoolDetailsPresenter.Section.sat.rawValue], with: .automatic)
        case (3, .none):
            tableView.deleteSections([SchoolDetailsPresenter.Section.sat.rawValue], with: .automatic)
        case (3, .some(_)):
            tableView.reloadSections([SchoolDetailsPresenter.Section.sat.rawValue], with: .automatic)
        default: break
        }
    }
    
    func didFailToLoadSatScore(message: String) {
        let action = UIAlertAction(title: Localizable.Common.Alert.Action.Confirm.title, style: .cancel, handler: nil)
        let alertController = UIAlertController(title: Localizable.Common.Alert.TechicalError.title, message: message, preferredStyle: .alert)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
