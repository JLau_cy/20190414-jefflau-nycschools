//
//  ProgressTableViewCell.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

final class ProgressTableViewCell: UITableViewCell {
    static let cellId = "ProgressCellId"
    
    @IBOutlet weak var mathDescriptionLabel: UILabel!
    @IBOutlet weak var mathProgressView: UIProgressView!
    @IBOutlet weak var readingDescriptionLabel: UILabel!
    @IBOutlet weak var readingProgressView: UIProgressView!
    @IBOutlet weak var writingDescriptionLabel: UILabel!
    @IBOutlet weak var writingProgressView: UIProgressView!

    
    func configure(mathDescription: String,
                   mathProgress: Float,
                   readingDescription: String,
                   readingProgress: Float,
                   writingDescription: String,
                   writingProgress: Float) {
        mathDescriptionLabel.text = mathDescription
        mathProgressView.setProgress(mathProgress, animated: true)
        readingDescriptionLabel.text = readingDescription
        readingProgressView.setProgress(readingProgress, animated: true)
        writingDescriptionLabel.text = writingDescription
        writingProgressView.setProgress(writingProgress, animated: true)
    }
}
