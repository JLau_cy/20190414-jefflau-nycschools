//
//  SchoolDetailsTableViewDatasource.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

final class SchoolDetailsTableViewDataSource: NSObject, UITableViewDataSource {
    weak var presenter: SchoolDetailsPresenter?
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfRows(in: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return presenter?.title(for: section) ?? ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let presenter = presenter else { return UITableViewCell() }
        let row = presenter.row(for: indexPath)
        switch row {
        case .description:
            let cell: DescriptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: DescriptionTableViewCell.cellId, for: indexPath)
            cell.configure(with: presenter.schoolDescription)
            return cell
        case .website where presenter.schoolWebsite != nil:
            let cell: ImageAndTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: ImageAndTitleTableViewCell.cellId, for: indexPath)
            cell.configure(with: presenter.schoolWebsite!, icon: Asset.website.image)
            return cell
        case .phone:
            let cell: ImageAndTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: ImageAndTitleTableViewCell.cellId, for: indexPath)
            cell.configure(with: presenter.schoolPhoneNumber, icon: Asset.phone.image)
            return cell
        case .email where presenter.schoolEmail != nil:
            let cell: ImageAndTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: ImageAndTitleTableViewCell.cellId, for: indexPath)
            cell.configure(with: presenter.schoolEmail!, icon: Asset.email.image)
            return cell
        case .address:
            let cell: ImageAndTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: ImageAndTitleTableViewCell.cellId, for: indexPath)
            cell.configure(with: presenter.schoolAddress, icon: Asset.location.image)
            return cell
        case .map:
            let cell: MapTableViewCell = tableView.dequeueReusableCell(withIdentifier: MapTableViewCell.cellId, for: indexPath)
            cell.addSchoolAnnotationToMap(presenter.schoolMapLocation()!)
            return cell
        case .sat where presenter.satScoreDescription != nil:
            let cell: ProgressTableViewCell = tableView.dequeueReusableCell(withIdentifier: ProgressTableViewCell.cellId, for: indexPath)
            cell.configure(mathDescription: presenter.satScoreDescription!.mathDescription,
                           mathProgress: presenter.satScoreDescription!.mathPercentage,
                           readingDescription: presenter.satScoreDescription!.readingDescription,
                           readingProgress: presenter.satScoreDescription!.readingPercentage,
                           writingDescription: presenter.satScoreDescription!.writingDescription,
                           writingProgress: presenter.satScoreDescription!.writingPercentage)
            return cell
        default: return UITableViewCell()
        }
    }
}
