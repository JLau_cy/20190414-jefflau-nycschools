//
//  SchoolSplitViewController.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

final class SchoolSplitViewController: UISplitViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        preferredDisplayMode = .allVisible
    }
}

extension SchoolSplitViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        //Shows master view controller on first launch
        guard let topAsDetailController = secondaryViewController as? SchoolDetailsViewController else { return false }
        if topAsDetailController.presenter == nil { return true }
        return false
    }
}
