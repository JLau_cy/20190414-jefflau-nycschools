//
//  SchoolDetailsPresenter.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol SchoolDetailsPresenterDelegate: class {
    func didLoadSatScore(_ score: SatScore?, isFromCache: Bool)
    func didFailToLoadSatScore(message: String)
}

final class SchoolDetailsPresenter {
    
    /// Enum describing the row types for school details
    enum Row {
        case description, website, phone, email, address, map, sat
    }
    
    /// Enum describing the section types for school details
    enum Section: Int {
        case description, information, sat
        
        var title: String {
            switch self {
            case .information: return Localizable.Schools.Detail.Information.Section.title
            case .sat: return Localizable.Schools.Detail.Sat.Section.title
            default: return ""
            }
        }
    }
    
    struct SatScoreDescription {
        let mathDescription: String
        let mathPercentage: Float
        let readingDescription: String
        let readingPercentage: Float
        let writingDescription: String
        let writingPercentage: Float
        
        init(satScore: SatScore) {
            let maxSatScore: Float = 800
            mathDescription = Localizable.Schools.Detail.Sat.Math.title(Float(satScore.math))
            readingDescription = Localizable.Schools.Detail.Sat.Reading.title(Float(satScore.reading))
            writingDescription = Localizable.Schools.Detail.Sat.Writing.title(Float(satScore.writing))
            mathPercentage = Float(satScore.math) / maxSatScore
            readingPercentage = Float(satScore.reading) / maxSatScore
            writingPercentage = Float(satScore.writing) / maxSatScore
        }
    }
    
    weak var delegate: SchoolDetailsPresenterDelegate?
    
    lazy var title: String = school.name
    lazy var schoolDescription = school.overview
    lazy var schoolPhoneNumber: String = school.phoneNumber
    lazy var schoolEmail: String? = school.email
    lazy var schoolWebsite: String? = school.website?.absoluteString
    
    lazy var schoolAddress: String = {
        return "\(school.addressLine1), \(school.neighborhood), \(school.city) \(school.stateCode) \(school.zip)"
    }()
    
    lazy var schoolCoordinate: CLLocationCoordinate2D? = {
        guard let lat = school.latitude,
            let lon = school.longitude else { return nil }
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }()
    
    lazy var informationSectionOrder: [Row] = {
        var rowOrder: [Row] = [.website, .phone, .email, .address, .map]
        if school.website == nil {
            let index = rowOrder.firstIndex(of: .website)!
            rowOrder.remove(at: index)
        }
        if school.email == nil {
            let index = rowOrder.firstIndex(of: .email)!
            rowOrder.remove(at: index)
        }
        if schoolCoordinate == nil {
            let index = rowOrder.firstIndex(of: .map)!
            rowOrder.remove(at: index)
        }
        return rowOrder
    }()
    
    var numberOfSections: Int {
        var numberOfSections = 3
        if school.satScore == nil { numberOfSections -= 1 }
        return numberOfSections
    }
    
    var satScoreDescription: SatScoreDescription?
    
    private var school: School
    
    init(school: School) {
        self.school = school
    }
    
    func schoolMapLocation() -> (coordinate: CLLocationCoordinate2D, latDistance: Double, lonDistance: Double)? {
        guard schoolCoordinate != nil else { return nil }
        return (schoolCoordinate!, 500, 500)
    }
    
    /// Requests SAT scores from API and informs delegate of the result.
    func loadSatScores(urlSession: URLSession = URLSession.shared) {
        // If school in cache already has SAT scores then just return from cache
        if let score = school.satScore {
            DispatchQueue.main.async { [weak self] in
                self?.satScoreDescription = SatScoreDescription(satScore: score)
                self?.delegate?.didLoadSatScore(score, isFromCache: true)
            }
            return
        } else {
            let satScoreRequest = GetSatScoreRequest(dbn: school.dbn)
            satScoreRequest.start(urlSession) { [weak self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let scores) where scores.count == 1:
                        let score = scores.first!
                        self?.school.satScore = score
                        self?.satScoreDescription = SatScoreDescription(satScore: score)
                        self?.delegate?.didLoadSatScore(score, isFromCache: false)
                    case .success(let scores) where scores.count == 0:
                        self?.delegate?.didLoadSatScore(nil, isFromCache: false)
                    case .failure(let error):
                        self?.delegate?.didFailToLoadSatScore(message: error.localizedDescription)
                    default:
                        self?.delegate?.didFailToLoadSatScore(message: "Unknown error")
                    }
                }
            }
        }
    }
    
    func numberOfRows(in section: Int) -> Int {
        let section = Section(rawValue: section)
        switch section {
        case .some(.description): return 1
        case .some(.information): return informationSectionOrder.count
        case .some(.sat) where school.satScore != nil: return 1
        default: return 0
        }
    }
    
    func title(for section: Int) -> String {
        let section = Section(rawValue: section)
        switch section {
        case .some(.description): return school.name
        case .some(.information): return section!.title
        case .some(.sat): return section!.title
        default: return ""
        }
    }
    
    func row(for indexPath: IndexPath) -> Row {
        guard let section = Section(rawValue: indexPath.section) else {
            fatalError("DEVELOPER ERROR: Unhandled section index! Check number of sections.")
        }
        switch section {
        case .description: return .description
        case .information where indexPath.row < informationSectionOrder.count: return informationSectionOrder[indexPath.row]
        case .sat: return .sat
        default:
            fatalError("DEVELOPER ERROR: Index out of bounds!")
        }
    }
}
