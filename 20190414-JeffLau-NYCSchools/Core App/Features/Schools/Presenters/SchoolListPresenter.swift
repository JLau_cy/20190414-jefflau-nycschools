//
//  SchoolListPresenter.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

protocol SchoolListPresenterDelegate: class {
    func didLoadSchools(_ schools: [School])
    func didFailToLoadSchools(message: String)
}

final class SchoolListPresenter {
    weak var delegate: SchoolListPresenterDelegate?

    let title = Localizable.Schools.List.Navigation.title
    
    private var cache: AppSessionCacheRepresentable
    
    init(cache: AppSessionCacheRepresentable = AppSessionCache.shared) {
        self.cache = cache
    }
    
    /// Requests schools from API and informs delegate of the result.
    @objc func loadSchools(urlSession: URLSession = URLSession.shared) {
        let schoolsRequest = GetSchoolsRequest()
        schoolsRequest.start(urlSession) { [weak self] result in
            guard let unwrappedSelf = self else { return }
            switch result {
            case .success(let schools):
                unwrappedSelf.cache.schools = schools
                DispatchQueue.main.async {
                    unwrappedSelf.delegate?.didLoadSchools(unwrappedSelf.cache.schools!)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    unwrappedSelf.delegate?.didFailToLoadSchools(message: error.localizedDescription)
                }
            }
        }
    }
    
    func numberOfSchools() -> Int {
        return cache.schools?.count ?? 0
    }
    
    /// Returns a tuple containing a school name and school's neighborhood
    ///
    /// - Parameter index: The index of the school in the cache
    /// - Returns: An optional tuple containing a school name and school's neighborhood or nil if school is not found.
    func schoolNameAndNeighborhood(for index: Int) -> (name: String, neighborhood: String)? {
        guard let school = school(at: index) else { return nil }
        return (school.name, school.neighborhood)
    }
    
    /// Returns an optional SchoolDetailsPresenter instance for a given index
    ///
    /// - Parameter index: The index location of a school
    /// - Returns: The SchoolDetailsPresenter instance. nil if schools cache is nil or empty
    func detailsPresenter(for index: Int) -> SchoolDetailsPresenter? {
        guard let schools = cache.schools, schools.count > index else { return nil }
        let school = schools[index]
        return SchoolDetailsPresenter(school: school)
    }
    
    /// Returns a school from the cache
    ///
    /// - Parameter index: The index of a school.
    /// - Returns: Returns a school instance from the cache or nil if no cache or invalid index.
    private func school(at index: Int) -> School? {
        guard let schools = cache.schools, schools.count > index else { return nil }
        return schools[index]
    }
}
