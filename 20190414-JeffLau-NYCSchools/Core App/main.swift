//
//  main.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import UIKit

/// Swaps out AppDelegate implementation if TestingAppDelegate class exists.
/// This indicates unit tests are running and avoids normal application start sequence to speed up tests.
let appDelegateClass: AnyClass =
    NSClassFromString("TestingAppDelegate") ?? AppDelegate.self

UIApplicationMain(
    CommandLine.argc,
    CommandLine.unsafeArgv,
    nil,
    NSStringFromClass(appDelegateClass)
)
