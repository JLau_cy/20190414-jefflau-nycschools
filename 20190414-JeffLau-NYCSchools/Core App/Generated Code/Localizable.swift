// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum Localizable {
  internal enum Common {
    internal enum Alert {
      internal enum Action {
        internal enum Confirm {
          /// Okay
          internal static let title = Localizable.tr("Common", "alert.action.confirm.title")
        }
      }
      internal enum TechicalError {
        /// Technical Error
        internal static let title = Localizable.tr("Common", "alert.techicalError.title")
      }
    }
  }
  internal enum Schools {
    internal enum Detail {
      internal enum Information {
        internal enum Section {
          /// Information
          internal static let title = Localizable.tr("Schools", "detail.information.section.title")
        }
      }
      internal enum Sat {
        internal enum Math {
          /// Math - %.0f
          internal static func title(_ p1: Float) -> String {
            return Localizable.tr("Schools", "detail.sat.math.title", p1)
          }
        }
        internal enum Reading {
          /// Reading - %.0f
          internal static func title(_ p1: Float) -> String {
            return Localizable.tr("Schools", "detail.sat.reading.title", p1)
          }
        }
        internal enum Section {
          /// SAT Scores
          internal static let title = Localizable.tr("Schools", "detail.sat.section.title")
        }
        internal enum Writing {
          /// Writing - %.0f
          internal static func title(_ p1: Float) -> String {
            return Localizable.tr("Schools", "detail.sat.writing.title", p1)
          }
        }
      }
    }
    internal enum List {
      internal enum Navigation {
        /// Schools
        internal static let title = Localizable.tr("Schools", "list.navigation.title")
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension Localizable {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
