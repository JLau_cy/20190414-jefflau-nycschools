//
//  SchoolGettable.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

struct GetSchoolsRequest: HttpGettable {
    typealias DecodableType = [School]
    
    let baseUrl = CityOfNewYorkUrl.base
    let path = CityOfNewYorkUrl.school.rawValue
    
    //Filtering and ordering API to only return information I need to display.
    var params: [String : String]? = ["$select": "dbn,school_name,neighborhood,overview_paragraph,longitude,latitude,website,city,state_code,phone_number,primary_address_line_1,school_email,zip",
                                      "$order": "school_name"
    ]
}
