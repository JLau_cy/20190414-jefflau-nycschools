//
//  BaseUrl.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

/// Enum to hold all data.cityofnewyork.us related paths used in the app
enum CityOfNewYorkUrl: String {
    static let base = "https://data.cityofnewyork.us/"
    case school = "resource/s3k6-pzi2.json"
    case sat = "resource/f9bf-2cp4.json"
}
