//
//  GetSatScoreRequest.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

struct GetSatScoreRequest: HttpGettable {
    typealias DecodableType = [SatScore]
    
    let baseUrl = CityOfNewYorkUrl.base
    let path = CityOfNewYorkUrl.sat.rawValue
    
    //Filtering API to only return information I need to display.
    var params: [String : String]? = ["$select": "sat_math_avg_score,sat_critical_reading_avg_score,sat_writing_avg_score"]
    
    init(dbn: String) {
        params?["dbn"] = dbn
    }
}
