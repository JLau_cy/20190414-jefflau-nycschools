//
//  AppSessionCache.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

/// Protocol describing the structure of an AppSessionCache
/// The purpose of extracting AppSessionCache functionality into a protocol is to aid testing of a singleton instance
protocol AppSessionCacheRepresentable {
    var schools: [School]? { get set }
}

/// A signleton to hold shared in memory data persisting throughout the app's session.
/// If the app supported offline access, CoreData would replace this class.
/// Although CoreData does support in memory data management.
/// I believe for the requirements and scale of this app the overhead of managing a CoreData stack would be unnecessary and provides little benefits over a singleton approach.
final class AppSessionCache: AppSessionCacheRepresentable {
    static let shared = AppSessionCache()
    
    var schools: [School]?
}
