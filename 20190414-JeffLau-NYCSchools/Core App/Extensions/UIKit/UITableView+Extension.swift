//
//  UITableView+Extension.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 13/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    /// Same as `dequeueReusableCell(withIdentifier:indexPath)` from UIKit with the addition of returning a already casted UITableViewCell subclass.
    func dequeueReusableCell<T>(withIdentifier: String, for indexPath: IndexPath) -> T where T: UITableViewCell {
        guard let cell = dequeueReusableCell(withIdentifier: withIdentifier, for: indexPath) as? T else {
            fatalError("DEVELOPER ERROR: Incorrect cell type for identifier \(withIdentifier)")
        }
        return cell
    }
}
