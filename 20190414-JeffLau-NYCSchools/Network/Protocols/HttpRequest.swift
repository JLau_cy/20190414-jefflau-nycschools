//
//  HttpRequest.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

/// Protocol describing the structure of a HTTP request
protocol HttpRequest {
    //The expected return result type of this HTTP request after successful data decoding from a data task.
    associatedtype DecodableType: Decodable
    
    var method: String { get }
    var baseUrl: String { get }
    var path: String { get }
    var params: [String: String]? { get set }
    var timeout: Double { get }
    var request: URLRequest { get }
    
    func start(_ session: URLSession, resultHandler: @escaping (Result<DecodableType, HttpResponseError>) -> Void)
}

extension HttpRequest {
    
    ///Takes the properties of a HttpRequest conforming type and returns a URLRequest
    var request: URLRequest {
        guard let url = buildURL() else {
            fatalError("DEVELOPER ERROR - The url should never return nil, check baseUrl and UrlPath enums aswell as params value")
        }
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.timeoutInterval = timeout
        
        return request
    }
    
    /// Using a URLSession, begins a data task using the HttpRequest conforming type's request property
    ///
    /// - Parameters:
    ///   - session: The URLSession to use, defaults to URLSession.shared.
    ///   - resultHandler: The result handler after the data task completes or fails.
    func start(_ session: URLSession = URLSession.shared, resultHandler: @escaping (Result<DecodableType, HttpResponseError>) -> Void) {
        session.dataTask(with: request) { data, response, error in
            let responseError = self.isResponseValid(data: data, response: response, error: error)
            guard responseError == nil else {
                resultHandler(.failure(responseError!))
                return
            }
            do {
                let decoded = try JSONDecoder().decode(DecodableType.self, from: data!)
                resultHandler(.success(decoded))
            } catch {
                resultHandler(.failure(HttpResponseError.cannotDecodeData))
            }
            
            }.resume()
    }
    
    /// Perfoms standard HTTP reponse checks against the return values of a data task to ensure it is safe to procceed with data decoding.
    ///
    /// - Parameters:
    ///   - data: An optional data object from a data task
    ///   - response: An optional URLResponse object from a data task
    ///   - error: An optional error object from a data task
    /// - Returns: A HttpResponseError if an error is found otherwise nil to indicate no errors found.
    private func isResponseValid(data: Data?, response: URLResponse?, error: Error?) -> HttpResponseError? {
        guard error == nil else {
            //For this excerise the app only cares about timeout and connection error code.
            //All other error codes will be treated as an unknown error.
            switch (error! as NSError).code {
            case NSURLErrorTimedOut: return HttpResponseError.timeout
            case NSURLErrorNotConnectedToInternet: return HttpResponseError.noInternet
            default: return HttpResponseError.unknown
            }
        }
        
        guard let httpResponse = response as? HTTPURLResponse else { return HttpResponseError.notHttpResponse }
        
        //For this excerise the app only cares about 200 range success codes.
        //All other status codes will be treated as an error
        guard (200...299).contains(httpResponse.statusCode) else {
            switch httpResponse.statusCode {
            case 100...199: return HttpResponseError.information
            case 300...399: return HttpResponseError.redirect
            case 400...499: return HttpResponseError.client
            case 500...599: return HttpResponseError.server
            default: return HttpResponseError.unknown
            }
        }
        
        guard data != nil,
            data!.count > 0  else { return HttpResponseError.noData }
        
        return nil
    }
    
    /// Builds a URL using the values of baseUrl, path and params.
    ///
    /// - Returns: An optional URL
    private func buildURL() -> URL? {
        var urlString = baseUrl + path
        
        if let params = params {
            urlString += "?"
            for (index, param) in params.enumerated() {
                if index != 0 { urlString += "&" }
                urlString += "\(param.key)=\(param.value)"
            }
        }
        
        return URL(string: urlString)
    }
}
