//
//  HttpGettable.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

/// Protocol to represnet a HTTP GET request.
protocol HttpGettable: HttpRequest {}

extension HttpGettable {
    var method: String { return HttpMethod.get.rawValue }
    var timeout: Double { return 10 }
}
