//
//  HTTPMethod.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation


/// Enum describing HTTP methods.
/// For this app I am only concerned about GET.
/// In practice this would consist of other HTTP methods such as POST, PUT, DELETE depending on app needs.
enum HttpMethod: String {
    case get = "GET"
}
