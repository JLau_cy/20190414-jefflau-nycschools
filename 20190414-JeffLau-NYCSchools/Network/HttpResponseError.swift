//
//  HttpResponseError.swift
//  20190414-JeffLau-NYCSchools
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation

/// Enum describing HTTP response errors that the app will handle.
enum HttpResponseError: Error {
    case timeout
    case information
    case redirect
    case client
    case server
    case noData
    case notHttpResponse
    case cannotDecodeData
    case noInternet
    case unknown
    
    var localizedDescription: String {
        switch self {
        case .timeout: return "Request timed out"
        case .information: return "HTTP status code range 100"
        case .redirect: return "HTTP status code range 300"
        case .client: return "HTTP status code range 400"
        case .server: return "HTTP status code range 500"
        case .noData: return "No data returned from request"
        case .notHttpResponse: return "Missing HTTP reponse"
        case .cannotDecodeData: return "Data decoding error"
        case .noInternet: return "Check internet connection"
        case .unknown: return "Unknown error"
        }
    }
}
