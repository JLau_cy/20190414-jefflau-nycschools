//
//  SchoolModelTests.swift
//  20190414-JeffLau-NYCSchoolsTests
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import XCTest
@testable import NYC_Schools

class SchoolModelTests: XCTestCase {    
    func test_decode_givenValidJsonData_returnsSchoolInstance() {
        let validSchoolJsonData = Stub.singleSchoolJson.rawValue.data(using: .utf8)!
        do {
            _ = try JSONDecoder().decode(School.self, from: validSchoolJsonData)
            XCTAssertTrue(true)
        } catch {
            XCTFail("Expected school object to be decoded")
        }
    }
}
