//
//  SatModelTests.swift
//  20190414-JeffLau-NYCSchoolsTests
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import XCTest
@testable import NYC_Schools

class SatModelTests: XCTestCase {
    
    func test_decode_givenValidJsonData_returnsSatScoreInstance() {
        let validSatScoreJsonData = Stub.singleSatScoreJson.rawValue.data(using: .utf8)!
        do {
            _ = try JSONDecoder().decode(SatScore.self, from: validSatScoreJsonData)
            XCTAssertTrue(true)
        } catch {
            XCTFail("Expected SatScore object to be decoded")
        }
    }
}
