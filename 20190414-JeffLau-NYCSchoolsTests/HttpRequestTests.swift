//
//  _0190414_JeffLau_NYCSchoolsTests.swift
//  20190414-JeffLau-NYCSchoolsTests
//
//  Created by Jeff Lau on 12/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class HttpRequestTests: XCTestCase {
    
    var urlSession: URLSession {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [URLProtocolMock.self]
        let urlSession = URLSession(configuration: configuration)
        return urlSession
    }
    
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        URLProtocolMock.requestHandler = nil
        super.tearDown()
    }
    
    func test_start_givenTimeoutError_returnsHttpResponseErrorTimeout() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let error = NSError(domain: "", code: NSURLErrorTimedOut, userInfo: nil)
            return (nil, nil, error)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .timeout)
            default:
                XCTFail("Expected timeout error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_givenNotConnectedToInternetError_returnsHttpResponseErrorNoInternet() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
            return (nil, nil, error)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .noInternet)
            default:
                XCTFail("Expected no internet error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_givenOtherError_returnsHttpResponseErrorUnknown() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let error = NSError(domain: "", code: 1, userInfo: nil)
            return (nil, nil, error)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .unknown)
            default:
                XCTFail("Expected unknown error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_given100StatusCode_returnsHttpResponseErrorInformation() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: requestMock.request.url!, statusCode: 100, httpVersion: nil, headerFields: nil)
            return (response, nil, nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .information)
            default:
                XCTFail("Expected information error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_given300StatusCode_returnsHttpResponseErrorInformation() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: requestMock.request.url!, statusCode: 300, httpVersion: nil, headerFields: nil)
            return (response, nil, nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .redirect)
            default:
                XCTFail("Expected redirect error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_given400StatusCode_returnsHttpResponseErrorClient() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: requestMock.request.url!, statusCode: 400, httpVersion: nil, headerFields: nil)
            return (response, nil, nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .client)
            default:
                XCTFail("Expected client error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_given500StatusCode_returnsHttpResponseErrorServer() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: requestMock.request.url!, statusCode: 500, httpVersion: nil, headerFields: nil)
            return (response, nil, nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .server)
            default:
                XCTFail("Expected server error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_givenNonStandardStatusCode_returnsHttpResponseErrorUnknown() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: requestMock.request.url!, statusCode: 700, httpVersion: nil, headerFields: nil)
            return (response, nil, nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .unknown)
            default:
                XCTFail("Expected unknow error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_givenMissingResponse_returnsHttpResponseErrorNoHttpResponse() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            return (nil, nil, nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .notHttpResponse)
            default:
                XCTFail("Expected no http response error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_givenSuccessStatusCodeMissingData_returnsHttpResponseErrorNoData() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: requestMock.request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)
            return (response, nil, nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .noData)
            default:
                XCTFail("Expected missing data error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_givenInvalidData_returnsHttpResponseErrorCannotDecodeData() {
        let requestMock = GetHttpRequestMock()
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: requestMock.request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)
            return (response, "Invalid data".data(using: .utf8), nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, .cannotDecodeData)
            default:
                XCTFail("Expected cannot decode data error")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_start_givenSuccessStatusCodeAndValidData_returnsSuccessWithDecodedObject() {
        let requestMock = GetHttpRequestMock()
        let resultDict = ["Result":"Success"]
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: requestMock.request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)
            
            let resultData = try! JSONSerialization.data(withJSONObject: resultDict, options: .prettyPrinted)
            return (response, resultData, nil)
        }
        let expectation = XCTestExpectation()
        requestMock.start(urlSession) { result in
            switch result {
            case .success(let object):
                XCTAssertEqual(object, resultDict)
            default:
                XCTFail("Expected \(resultDict)")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
}
