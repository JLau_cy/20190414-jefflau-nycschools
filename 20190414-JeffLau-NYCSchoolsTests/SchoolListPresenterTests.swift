//
//  SchoolListPresenterTests.swift
//  20190414-JeffLau-NYCSchoolsTests
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import XCTest
@testable import NYC_Schools

class SchoolListPresenterTests: XCTestCase {
    var cache: AppSessionCacheMock!
    var expectation: XCTestExpectation?
    var expectationHandler: ((Any?) -> Void)?
    var presenter: SchoolListPresenter?
    
    var urlSession: URLSession {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [URLProtocolMock.self]
        let urlSession = URLSession(configuration: configuration)
        return urlSession
    }
    
    override func setUp() {
        super.setUp()
        let validSchoolJsonData = Stub.singleSchoolJson.rawValue.data(using: .utf8)!
        do {
            let schools = try JSONDecoder().decode(School.self, from: validSchoolJsonData)
            cache = AppSessionCacheMock(schools: [schools])
            presenter = SchoolListPresenter(cache: cache)
            presenter!.delegate = self
        } catch {
            XCTFail("Expected school array to be decoded")
        }
    }
    
    override func tearDown() {
        cache = nil
        expectation = nil
        expectationHandler = nil
        presenter = nil
        super.tearDown()
    }
    
    func test_loadSchools_givenEmptySchoolsCache_didLoadSchoolsCalledAndCacheIsPopulatedWithSchools() {
        cache = AppSessionCacheMock(schools: [])
        let presenter = SchoolListPresenter(cache: cache!)
        presenter.delegate = self
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: URL(string: "test@apple.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)
            let validSchoolJsonData = Stub.schoolArrayJson.rawValue.data(using: .utf8)!
            return (response, validSchoolJsonData, nil)
        }
        expectationHandler = { _ in
            XCTAssertNotNil(self.cache.schools, "Schools in cache should not be nil")
            XCTAssertEqual(self.cache.schools?.count, 1, "School count should equal 1")
            self.expectation?.fulfill()
        }
        expectation = XCTestExpectation()
        presenter.loadSchools(urlSession: urlSession)
        wait(for: [expectation!], timeout: 1.0)
    }
    
    func test_loadSchools_givenError_didFailToLoadSchoolsCalled() {
        cache = AppSessionCacheMock(schools: [])
        let presenter = SchoolListPresenter(cache: cache!)
        presenter.delegate = self
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: URL(string: "test@apple.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)
            return (response, nil, nil)
        }
        expectationHandler = { message in
            XCTAssertEqual(message! as! String, HttpResponseError.noData.localizedDescription)
            self.expectation?.fulfill()
        }
        expectation = XCTestExpectation()
        presenter.loadSchools(urlSession: urlSession)
        wait(for: [expectation!], timeout: 1.0)
    }
    
    func test_numberOfSchools_givenHasOneSchool_returnsOne() {
        let numberOfSchools = presenter?.numberOfSchools()
        XCTAssertEqual(numberOfSchools, 1, "Number of schools should be 1")
    }
    
    func test_numberOfSchools_givenSchoolsInCacheIsNil_returnsZero() {
        cache.schools = nil
        let numberOfSchools = presenter?.numberOfSchools()
        XCTAssertEqual(numberOfSchools, 0, "Number of schools should be 0")
    }
    
    func test_schoolNameAndNeighborhood_givenSchoolExistAtIndexZero_returnsNameAndNeighborhoodTuple() {
        let nameAndNeighborhoodTuple = presenter?.schoolNameAndNeighborhood(for: 0)
        let tuple = ("Â“47Â” The American Sign Language and English Secondary School", "Gramercy")
        XCTAssertNotNil(nameAndNeighborhoodTuple, "Expected not to be nil")
        XCTAssertEqual(nameAndNeighborhoodTuple?.name, tuple.0, "Expected name: \(tuple.0)")
        XCTAssertEqual(nameAndNeighborhoodTuple?.neighborhood, tuple.1, "Expected neighborhood: \(tuple.1)")
    }
    
    func test_schoolNameAndNeighborhood_givenSchoolDoesNotExistAtIndex_returnsNil() {
        let nameAndNeighborhoodTuple = presenter?.schoolNameAndNeighborhood(for: 1)
        XCTAssertNil(nameAndNeighborhoodTuple, "Expected to be nil")
    }
    
    func test_detailsPresenter_givenSchoolExistAtIndexZero_returnsSchoolDetailsPresenter() {
        let schoolDetailsPresenter = presenter?.detailsPresenter(for: 0)
        XCTAssertNotNil(schoolDetailsPresenter, "Expected not to be nil")
    }
    
    func test_detailsPresenter_givenOutOfBandIndex_returnsNil() {
        let schoolDetailsPresenter = presenter?.detailsPresenter(for: 1)
        XCTAssertNil(schoolDetailsPresenter, "Expected to be nil")
    }
}

extension SchoolListPresenterTests: SchoolListPresenterDelegate {
    func didLoadSchools(_ schools: [School]) {
        expectationHandler?(nil)
    }
    
    func didFailToLoadSchools(message: String) {
        expectationHandler?(message)
    }
}
