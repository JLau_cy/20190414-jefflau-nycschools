//
//  SchoolDetailsPresenterTests.swift
//  20190414-JeffLau-NYCSchoolsTests
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import CoreLocation
import XCTest
@testable import NYC_Schools

class SchoolDetailsPresenterTests: XCTestCase {        
    var expectation: XCTestExpectation?
    var expectationHandler: ((Any?) -> Void)?
    var presenter: SchoolDetailsPresenter?
    
    var urlSession: URLSession {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [URLProtocolMock.self]
        let urlSession = URLSession(configuration: configuration)
        return urlSession
    }
    
    override func setUp() {
        super.setUp()
        let validSchoolJsonData = Stub.singleSchoolJson.rawValue.data(using: .utf8)!
        do {
            let school = try JSONDecoder().decode(School.self, from: validSchoolJsonData)
            presenter = SchoolDetailsPresenter(school: school)
            presenter!.delegate = self
        } catch {
            XCTFail("Expected school array to be decoded")
        }
    }
    
    override func tearDown() {
        expectation = nil
        expectationHandler = nil
        presenter = nil
        super.tearDown()
    }
    
    func test_schoolAddress_givenFullyPopulatedSchool_returnsFullAddress() {
        let address = "223 East 23rd Street, Gramercy, Manhattan NY 10010"
        let schoolAddress = presenter?.schoolAddress
        XCTAssertEqual(schoolAddress, address, "Expected address to equal \(address)")
    }
    
    func test_schoolCoordinate_givenFullyPopulatedSchool_returnsCoordinate() {
        let coordinate = CLLocationCoordinate2D(latitude: 40.73837, longitude: -73.9813)
        let schoolCoordinate = presenter?.schoolCoordinate
        XCTAssertEqual(schoolCoordinate, coordinate, "Expected address to equal \(coordinate)")
    }
    
    func test_informationSectionOrder_givenFullyPopulatedSchool_returnsFullOrder() {
        let order: [SchoolDetailsPresenter.Row] = [.website, .phone, .email, .address, .map]
        let rowOrder = presenter?.informationSectionOrder
        XCTAssertEqual(rowOrder, order, "Expected row order to equal \(order)")
    }
    
    func test_informationSectionOrder_givenMissingWebsite_returnsOrderWithoutWebsite() {
        let invalidWebsiteJsonData = Stub.schoolMissingWebsiteJson.rawValue.data(using: .utf8)!
        let school = try! JSONDecoder().decode(School.self, from: invalidWebsiteJsonData)
        presenter = SchoolDetailsPresenter(school: school)
        let order: [SchoolDetailsPresenter.Row] = [.phone, .email, .address, .map]
        let rowOrder = presenter?.informationSectionOrder
        XCTAssertEqual(rowOrder, order, "Expected row order to equal \(order)")
    }
    
    func test_informationSectionOrder_givenMissingEmail_returnsOrderWithoutEmail() {
        let missingEmailJsonData = Stub.schoolMissingEmailJson.rawValue.data(using: .utf8)!
        let school = try! JSONDecoder().decode(School.self, from: missingEmailJsonData)
        presenter = SchoolDetailsPresenter(school: school)
        let order: [SchoolDetailsPresenter.Row] = [.website, .phone, .address, .map]
        let rowOrder = presenter?.informationSectionOrder
        XCTAssertEqual(rowOrder, order, "Expected row order to equal \(order)")
    }
    
    func test_informationSectionOrder_givenMissingCoordinate_returnsOrderWithoutMap() {
        let missingCoordinateJsonData = Stub.schoolMissingCoordinateJson.rawValue.data(using: .utf8)!
        let school = try! JSONDecoder().decode(School.self, from: missingCoordinateJsonData)
        presenter = SchoolDetailsPresenter(school: school)
        let order: [SchoolDetailsPresenter.Row] = [.website, .phone, .email, .address]
        let rowOrder = presenter?.informationSectionOrder
        XCTAssertEqual(rowOrder, order, "Expected row order to equal \(order)")
    }
    
    func test_numberOfSections_givenNoSatScoreInSchool_returnsTwo() {
        let numberOfSections = presenter?.numberOfSections
        XCTAssertEqual(numberOfSections, 2, "Expected 2, but got \(String(describing: numberOfSections))")
    }
    
    func test_numberOfSections_givenSatScorePresentInSchool_returnsThree() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let numberOfSections = presenter?.numberOfSections
        XCTAssertEqual(numberOfSections, 3, "Expected 3, but got \(String(describing: numberOfSections))")
    }
    
    func test_schoolMapLocation_givenCoordinatePresentInSchool_returnsMapLocationTuple() {
        let coordinate = CLLocationCoordinate2D(latitude: 40.73837, longitude: -73.9813)
        let tuple = (coordinate, 500.0, 500.0)
        let mapLocation = presenter?.schoolMapLocation()
        XCTAssertEqual(mapLocation?.coordinate, tuple.0, "Expected to equal \(String(describing: tuple.0))")
        XCTAssertEqual(mapLocation?.latDistance, tuple.1, "Expected to equal \(String(describing: tuple.1))")
        XCTAssertEqual(mapLocation?.lonDistance, tuple.2, "Expected to equal \(String(describing: tuple.2))")
    }
    
    func test_loadSatScores_givenSatScorePresentInSchool_callsDidLoadSatScoreIsFromCacheEqualsTrue() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        expectationHandler = { tuple in
            let tuple = tuple as! (SatScore?, Bool)
            XCTAssertEqual(tuple.1, true, "Expected true, but got \(tuple.1)")
            self.expectation?.fulfill()
        }
        expectation = XCTestExpectation()
        presenter?.loadSatScores(urlSession: urlSession)
        wait(for: [expectation!], timeout: 1.0)
    }
    
    func test_loadSatScores_givenSatScoreNotPresentInSchool_callsDidLoadSatScoreIsFromCacheEqualsFalse() {
        URLProtocolMock.requestHandler = { request in
            let response = HTTPURLResponse(url: URL(string: "test@apple.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)
            let validSatScoreJsonData = Stub.satScoreArrayJson.rawValue.data(using: .utf8)!
            return (response, validSatScoreJsonData, nil)
        }
        expectationHandler = { tuple in
            let tuple = tuple as! (SatScore?, Bool)
            XCTAssertNotNil(tuple.0, "SatScrore should not be nil")
            XCTAssertEqual(tuple.1, false, "Expected false, but got \(tuple.1)")
            self.expectation?.fulfill()
        }
        expectation = XCTestExpectation()
        presenter?.loadSatScores(urlSession: urlSession)
        wait(for: [expectation!], timeout: 10.0)
    }
    
    func test_numberOfRows_givenDescriptionSectionIndex_returnsOne() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let numberOfRows = presenter!.numberOfRows(in: SchoolDetailsPresenter.Section.description.rawValue)
        
        XCTAssertEqual(numberOfRows, 1, "Expected 1, but got \(numberOfRows)")
    }
    
    func test_numberOfRows_givenInformationSectionIndex_returnsFive() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let numberOfRows = presenter!.numberOfRows(in: SchoolDetailsPresenter.Section.information.rawValue)
        
        XCTAssertEqual(numberOfRows, 5, "Expected 5, but got \(numberOfRows)")
    }
    
    func test_numberOfRows_givenSatScoreSectionIndex_returnsOne() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let numberOfRows = presenter!.numberOfRows(in: SchoolDetailsPresenter.Section.sat.rawValue)
        
        XCTAssertEqual(numberOfRows, 1, "Expected 1, but got \(numberOfRows)")
    }
    
    func test_numberOfRows_givenSatScoreSectionIndexNoSatScore_returnsZero() {
        let numberOfRows = presenter!.numberOfRows(in: SchoolDetailsPresenter.Section.sat.rawValue)
        
        XCTAssertEqual(numberOfRows, 0, "Expected 0, but got \(numberOfRows)")
    }
    
    func test_numberOfRows_givenDescriptionSectionIndex_returnsExpectedTitle() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let title = presenter!.title(for: SchoolDetailsPresenter.Section.description.rawValue)
        let name = "Â“47Â” The American Sign Language and English Secondary School"
        XCTAssertEqual(title, name, "Expected \(name), but got \(title)")
    }
    
    func test_numberOfRows_givenInfomationSectionIndex_returnsExpectedTitle() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let title = presenter!.title(for: SchoolDetailsPresenter.Section.information.rawValue)
        let name = "Information"
        XCTAssertEqual(title, name, "Expected \(name), but got \(title)")
    }
    
    func test_numberOfRows_givenSatSectionIndex_returnsExpectedTitle() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let title = presenter!.title(for: SchoolDetailsPresenter.Section.sat.rawValue)
        let name = "SAT Scores"
        XCTAssertEqual(title, name, "Expected \(name), but got \(title)")
    }
    
    func test_row_givenDescriptionSectionIndexRow0_returnsDescriptionRow() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let indexPath = IndexPath(row: 0, section: SchoolDetailsPresenter.Section.description.rawValue)
        let row = presenter!.row(for: indexPath)
        XCTAssertEqual(row, .description, "Expected description, but got \(row)")
    }
    
    func test_row_givenSatSectionIndexRow0_returnsSatRow() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let indexPath = IndexPath(row: 0, section: SchoolDetailsPresenter.Section.sat.rawValue)
        let row = presenter!.row(for: indexPath)
        XCTAssertEqual(row, .sat, "Expected sat, but got \(row)")
    }
    
    func test_row_givenInformationSectionIndexRow0_returnsWebsiteRow() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let indexPath = IndexPath(row: 0, section: SchoolDetailsPresenter.Section.information.rawValue)
        let row = presenter!.row(for: indexPath)
        XCTAssertEqual(row, .website, "Expected sat, but got \(row)")
    }
    
    func test_row_givenInformationSectionIndexRow1_returnsPhoneRow() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let indexPath = IndexPath(row: 1, section: SchoolDetailsPresenter.Section.information.rawValue)
        let row = presenter!.row(for: indexPath)
        XCTAssertEqual(row, .phone, "Expected sat, but got \(row)")
    }
    
    func test_row_givenInformationSectionIndexRow2_returnsEmailRow() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let indexPath = IndexPath(row: 2, section: SchoolDetailsPresenter.Section.information.rawValue)
        let row = presenter!.row(for: indexPath)
        XCTAssertEqual(row, .email, "Expected sat, but got \(row)")
    }
    
    func test_row_givenInformationSectionIndexRow3_returnsAddressRow() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let indexPath = IndexPath(row: 3, section: SchoolDetailsPresenter.Section.information.rawValue)
        let row = presenter!.row(for: indexPath)
        XCTAssertEqual(row, .address, "Expected sat, but got \(row)")
    }
    
    func test_row_givenInformationSectionIndexRow3_returnsMapRow() {
        presenterWithFullyPopulatedSchoolAndSatScore()
        
        let indexPath = IndexPath(row: 4, section: SchoolDetailsPresenter.Section.information.rawValue)
        let row = presenter!.row(for: indexPath)
        XCTAssertEqual(row, .map, "Expected sat, but got \(row)")
    }
    
    private func presenterWithFullyPopulatedSchoolAndSatScore() {
        let validSchoolJsonData = Stub.singleSchoolJson.rawValue.data(using: .utf8)!
        let school = try! JSONDecoder().decode(School.self, from: validSchoolJsonData)
        let validSatScoreJsonData = Stub.singleSatScoreJson.rawValue.data(using: .utf8)!
        school.satScore = try! JSONDecoder().decode(SatScore.self, from: validSatScoreJsonData)
        presenter = SchoolDetailsPresenter(school: school)
        presenter?.delegate = self
    }
}

extension SchoolDetailsPresenterTests: SchoolDetailsPresenterDelegate {
    func didLoadSatScore(_ score: SatScore?, isFromCache: Bool) {
        expectationHandler?((score, isFromCache))
    }
    
    func didFailToLoadSatScore(message: String) {
        expectationHandler?(message)
    }
}

extension CLLocationCoordinate2D: Equatable {
    public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}
