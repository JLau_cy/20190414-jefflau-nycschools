//
//  URLProtocolMock.swift
//  20190414-JeffLau-NYCSchoolsTests
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
import XCTest

//Class to intercept HTTP calls for unit testing.
final class URLProtocolMock: URLProtocol {
    static var requestHandler: ((URLRequest) -> (HTTPURLResponse?, Data?, Error?))!
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        guard let handler = URLProtocolMock.requestHandler else {
            XCTFail("URLProtocolMock instance must have a request handler")
            return
        }
        
        let (response, data, error) = handler(request)
        
        if let error = error { client?.urlProtocol(self, didFailWithError: error) }
        if let response = response { client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed) }
        if let data = data { client?.urlProtocol(self, didLoad: data) }
        client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() {}
}
