//
//  AppSessionCacheMock.swift
//  20190414-JeffLau-NYCSchoolsTests
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
@testable import NYC_Schools

final class AppSessionCacheMock: AppSessionCacheRepresentable {
    var schools: [School]?
    
    init(schools: [School]) {
        self.schools = schools
    }
}
