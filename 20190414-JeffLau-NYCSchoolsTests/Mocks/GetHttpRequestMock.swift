//
//  GetSchoolsRequestMock.swift
//  20190414-JeffLau-NYCSchoolsTests
//
//  Created by Jeff Lau on 14/04/2019.
//  Copyright © 2019 Jeff Lau. All rights reserved.
//

import Foundation
@testable import NYC_Schools

struct GetHttpRequestMock: HttpGettable {
    typealias DecodableType = [String: String]
    
    let baseUrl = CityOfNewYorkUrl.base
    let path = CityOfNewYorkUrl.school.rawValue
    
    var params: [String : String]?
}
